cmake_minimum_required(VERSION 2.8.11)

file(RELATIVE_PATH NAME_OF_SOLUTION ${CMAKE_SOURCE_DIR}/.. ${CMAKE_SOURCE_DIR})

project(${NAME_OF_SOLUTION})

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

file(GLOB projects LIST_DIRECTORIES true "*")

foreach (projectname ${projects})
    if (IS_DIRECTORY ${projectname} AND EXISTS ${projectname}/CMakeLists.txt)
        ADD_SUBDIRECTORY(${projectname})
    endif(IS_DIRECTORY ${projectname} AND EXISTS ${projectname}/CMakeLists.txt)
ENDFOREACH(projectname)
