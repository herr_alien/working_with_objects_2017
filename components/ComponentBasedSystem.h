#pragma once

#include "Component.h"
#include <list>

class ComponentBasedSystem : public Component
{
    typedef std::list<IComponent*> ComponentsList;

    ComponentsList components;
public:
    explicit ComponentBasedSystem(double m);
    virtual ~ComponentBasedSystem();

    template <typename T>
    void GetComponent(T*& p)
    {
        p = nullptr;
        // loop through our components
        for (IComponent * comp : components)
        {
            // and check if there�s a component
            // of the required type
            p = dynamic_cast<T*>(comp);
            if (p != nullptr)
                break;
        }
    }

    void AddComponent(IComponent* c);
    bool UsesComponent(IComponent * c) const;

    double GetMass() const;
};
