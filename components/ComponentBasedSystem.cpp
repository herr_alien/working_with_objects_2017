#include "ComponentBasedSystem.h"

ComponentBasedSystem::ComponentBasedSystem(double m) : Component(m)
{

}

void ComponentBasedSystem::AddComponent(IComponent * c)
{
    components.push_back(c);
}

ComponentBasedSystem::~ComponentBasedSystem()
{
	for (IComponent * pComp : components)
	{
		delete pComp;
	}
}

double ComponentBasedSystem::GetMass() const 
{
    double fullMass = m_mass;
    for (const IComponent * comp : components)
        fullMass += comp->GetMass();

    return fullMass;
}

bool ComponentBasedSystem::UsesComponent(IComponent * c) const
{
    for (const IComponent * comp : components)
    {
        if (comp == c)
            return true;
    }
    return false;
}
