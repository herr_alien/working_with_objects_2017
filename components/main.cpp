#include <iostream>
#include <string>
#include "ComponentBasedSystem.h"
#include "Wheel.h"
#include "GroundSystem.h"
#include "PropulsionSystem.h"
#include "Engine.h"
#include "Batmobil.h"

int main()
{
	{
		std::cout << "Batmobile tests:" << std::endl;

        const double initialMass = 12;
		Batmobil batmobil(initialMass);

		{
			std::cout << "Default vehicle should have only the intial mass ";
			
            if (batmobil.GetMass() == initialMass)
				std::cout << "PASSED";
			else
				std::cout << "FAILED";

			std::cout << std::endl;
		}

		{
			std::cout << "Default vehicle - should be empty ";

			GroundSystem * carPart = nullptr;
			batmobil.GetComponent(carPart);
			
            PropulsionSystem * propulsion = nullptr;
            batmobil.GetComponent(propulsion);

            if (nullptr == carPart && nullptr == propulsion)
				std::cout << "PASSED";
			else
				std::cout << "FAILED";

			std::cout << std::endl;
		}

        const double groundSystemComponentMass = 12.5;
		{
			std::cout << "Create a ground vhicle ";
			GroundSystem * carPart = new GroundSystem(groundSystemComponentMass);
			batmobil.AddComponent(carPart);

            if (batmobil.UsesComponent(carPart))
                std::cout << "PASSED";
            else
                std::cout << "FAILED";

			std::cout << std::endl;
		}

        {
            std::cout << "Get the ground vehicle component ";
            GroundSystem * carPart = nullptr;
            batmobil.GetComponent(carPart);
            if (nullptr != carPart)
                std::cout << "PASSED";
            else
                std::cout << "FAILED";

            std::cout << std::endl;
        }

        {
            std::cout << "The batmobile is now heavier ";
            GroundSystem * carPart = nullptr;
            batmobil.GetComponent(carPart);

            double expectedMass = carPart->GetMass() + initialMass;

            if (batmobil.GetMass() == expectedMass)
                std::cout << "PASSED";
            else
                std::cout << "FAILED";

            std::cout << std::endl;
        }

		{
			std::cout << "The ground system gets some wheels ";
			GroundSystem * carPart = nullptr;
			batmobil.GetComponent(carPart);

			if (nullptr == carPart)
			{
				std::cout << "FAILED! - the batmobile lost it's ground system";
			}
			else
			{
				const int numberOfWheels = 6;
				bool allSixWheelsOK = true;
				for (int i = 0; i < numberOfWheels; i++)
				{
					const double wheelMass = 5;
					Wheel *w1 = new Wheel(wheelMass);
					carPart->AddComponent(w1);
					if (!carPart->UsesComponent(w1))
					{
						std::cout << "FAILED!";
						allSixWheelsOK = false;
						break;
					}
				}

				if (allSixWheelsOK)
					std::cout << "PASSED!";

				std::cout << std::endl;
			
			}


			std::cout << std::endl;
		}

		{
			std::cout << "Adding a useful propulsion system ";
			const double emptyPropulsionWeight = 25;
			PropulsionSystem * propulsion = new PropulsionSystem(emptyPropulsionWeight);

			batmobil.AddComponent(propulsion);
			if (!batmobil.UsesComponent(propulsion))
			{
				std::cout << "FAILED";
			}
			else
			{
				const double engineMass = 123;
				const double maxImpulse = 12345;
				Engine * engine = new Engine(engineMass, maxImpulse);

				propulsion->AddComponent(engine);
				if (!propulsion->UsesComponent(engine))
				{
					std::cout << "FAILED";
				}
				else
				{
					// add some wheels;
					Wheel * justAWheel = nullptr;
					GroundSystem * carPart = nullptr;
					batmobil.GetComponent(carPart);
					if (nullptr == carPart)
					{
						std::cout << "FAILED";
					}
					else
					{
						carPart->GetComponent(justAWheel);
						if (nullptr == justAWheel)
						{
							std::cout << "FAILED";
						}
						else
						{
							propulsion->AddComponent(justAWheel);
							if (propulsion->UsesComponent(justAWheel))
							{
								std::cout << "PASSED";
							}
							else
								std::cout << "FAILED";
						}
					}
				}
			}
			std::cout << std::endl;
		}

	
	}
	
	return 0;
}
