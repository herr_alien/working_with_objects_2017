cmake_minimum_required(VERSION 2.8.11)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

file(GLOB INCS "*.h")
file(GLOB SRCS "*.cpp")

# Tell CMake to create the helloworld executable

file(RELATIVE_PATH NAME_OF_PROJECT ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_LIST_DIR})

add_executable(${NAME_OF_PROJECT} ${SRCS} ${INCS})
