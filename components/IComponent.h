#pragma once

class IComponent 
{
public:
    virtual double GetMass() const = 0;
    virtual ~IComponent() {}
};
