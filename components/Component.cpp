#include "Component.h"

Component::Component(double m) : m_mass(m)
{
}

double Component::GetMass() const
{
    return m_mass;
}
