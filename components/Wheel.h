#pragma once

#include "Component.h"

class Wheel : public Component
{
public:
    explicit Wheel(double mass);
};
