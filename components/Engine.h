#pragma once

#include "Component.h"

class Engine : public Component
{
    double m_impulse;
public:
    Engine(double mass, double impulse);
    double ComputeVelocityIncrement(double totalMassOfVehicle) const;
};
