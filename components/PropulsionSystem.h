#pragma once
#include "ComponentBasedSystem.h"

class PropulsionSystem : public ComponentBasedSystem
{
public:
    PropulsionSystem(double m);
};
