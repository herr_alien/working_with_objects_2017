#pragma once

#include "IComponent.h"

class Component : public IComponent
{
protected:
    double m_mass;
public:
    explicit Component(double mass = 0.0);
    double GetMass() const;
};
