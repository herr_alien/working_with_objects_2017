#pragma once

#include "ComponentBasedSystem.h"

class Batmobil : public ComponentBasedSystem
{
   
public:
    explicit Batmobil(double m);

    double GetTopSpeed() const;
};
